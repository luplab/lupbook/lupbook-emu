# Config file
#
# Some variables need to be set to a certain value, such as
# CONFIG_RISCV_MAX_XLEN which can be 32 or 64.
#
# Other variables, used to toggle features, can either be set to 'y' or
# should be unset (e.g., commented out).

## X86 engine (only for temu)
CONFIG_X86EMU=y
## RISC-V architecture width
CONFIG_RISCV_MAX_XLEN=64
## WASM compilation
CONFIG_WASM=y
## Decompression methods
CONFIG_DECOMP_LZ4=y
CONFIG_DECOMP_ZSTD=y
