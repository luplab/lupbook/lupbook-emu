/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright (c) 2017 Fabrice Bellard
 * Copyright (c) 2020-2024 LupLab
 */

/*
 * This code implements functions called by the lupbook-emu emulator to get
 * access to the JS world.
 *
 * - `Module` is the object exported by emscripten
 * - `Module.VM` represents an expected VM API (for lupbook, it would be
 *   lupbook-vm) which offers the following functions: `register`,
 *   `consoleWrite`, `consoleGeometry`, `loadFile`, `fsExportFile`.
 */
mergeInto(LibraryManager.library, {
  /* Initialize connection between emulator and VM by registering emulator's API
   * (emulator functions that the VM can call) */
  js_init: function()
  {
    Module.VM.register({
      consoleQueue:
        (c) => Module.ccall('console_queue_char', null, ['number'], [c]),
      consoleResize:
        () => Module.ccall('console_resize_request'),
      fsImportFile:
        (fpath, fname, buf, len) => Module.ccall('fs_import_file', 'number',
          ['string', 'string', 'array', 'number'], [fpath, fname, buf, len]),
    });
  },

  /* Emit characters to VM's terminal from the emulator */
  console_write: function(opaque, buf, len)
  {
    /* Send actual byte values. It's up to the terminal on the other side to
     * support UTF-8 if applicable. */
    const str = String.fromCharCode.apply(String, HEAPU8.subarray(buf, buf + len));
    Module.VM.consoleWrite(str);
  },

  /* Retrieve VM's terminal size */
  console_get_size: function(pw, ph)
  {
    const console_geometry = Module.VM.consoleGeometry();
    HEAPU32[pw >> 2] = console_geometry.width;
    HEAPU32[ph >> 2] = console_geometry.height;
  },

  /* Export file from emulator's FS (following a request from the VM) */
  fs_export_file: function(fpathname, rc, buf, len)
  {
    const _fpathname = Module.AsciiToString(fpathname);

    if (rc !== 0) {
      /* The request from the VM failed */
      Module.VM.fsExportFile(_fpathname);
      return rc;
    }

    /* Export file to VM */
    const data = HEAPU8.subarray(buf, buf + len);
    Module.VM.fsExportFile(_fpathname, data);
    return 0;
  },

  /* Load a file into the emulator (typically at initialization time) */
  vm_load_file: function(pbuf, filename) {
    const _filename = Module.AsciiToString(filename);

    const array = Module.VM.loadFile(_filename);
    if (!array)
      return -1;

    const buf = _malloc(array.length);
    if (buf === 0)
      return -1;

    Module.writeArrayToMemory(array, buf);

    HEAPU32[pbuf >> 2] = buf;

    return array.length;
  }
});
