#
# TinyEMU - Makefile
#
# Copyright (c) 2016-2018 Fabrice Bellard
# Copyright (c) 2021 Joël Porquet-Lupine
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# Avoid builtin rules and variables
MAKEFLAGS += -rR


# Define all rule right away
all: native js


# Configuration
CONF_FILE ?= config.mk

## Include user config file
include $(CONF_FILE)

## Check config
ifeq ($(CONFIG_RISCV_MAX_XLEN),32)
else ifeq ($(CONFIG_RISCV_MAX_XLEN),64)
else
$(error Invalid token CONFIG_RISCV_MAX_XLEN)
endif


# Output
## Native environment
OUT := temu
## JS environment
OUT_JS := temu_rv$(CONFIG_RISCV_MAX_XLEN).js

## Build directory
ifneq ($(O),)
OUT_DIR := $(O)
else
OUT_DIR := $(CURDIR)
endif


# Compilation tools
CC=gcc
EMCC := emcc


# Compilation flags
## Common to native and JS environments
_cflags := -Wall		# Catch all warnings
_cflags += -MMD -MP		# Generate dependency files
_cflags += -std=gnu99	# C99 with GNU extensions
ifneq ($(D),1)
_cflags += -O2			# Optimization level
else
_cflags += -g			# Debug and no optimization
endif
_cflags += -DCONFIG_RISCV_MAX_XLEN=$(CONFIG_RISCV_MAX_XLEN)
ifdef CONFIG_DECOMP_ZSTD
_cflags += -DCONFIG_DECOMP_ZSTD
endif
ifdef CONFIG_DECOMP_LZ4
_cflags += -DCONFIG_DECOMP_LZ4
endif

_ldflags :=

## Native environment
CFLAGS := $(strip $(_cflags))
ifdef CONFIG_X86EMU
CFLAGS += -DCONFIG_X86EMU
endif
LDFLAGS := $(strip $(_ldflags))

## JS environment
_emldflags := --closure 0			# No closure compiler (otherwise enabled if >= -O3)
_emldflags += -s SINGLE_FILE=1		# Single output JS file
_emldflags += -s MODULARIZE=1		# Don't pollute global namespace
_emldflags += -s NO_EXIT_RUNTIME=1	# Does not quit JS runtime when main() completes
_emldflags += -s NO_FILESYSTEM=1	# No FS support from JS runtime
ifneq ($(CONFIG_JS_NAMESPACE),)		# Name of exported module
_emldflags += -s EXPORT_NAME="$(CONFIG_JS_NAMESPACE)"
endif
_emldflags += -s "EXPORTED_FUNCTIONS=[\
			  '_console_queue_char', '_console_resize_request',\
			  '_fs_import_file', '_main', '_malloc'\
			  ]"					# C functions called from JS
_emldflags += -s "EXPORTED_RUNTIME_METHODS=[\
			  'callMain', 'ccall', 'cwrap', \
			  'tryParseAsDataURI', 'AsciiToString', 'writeArrayToMemory' \
			  ]"					# Export runtime functions
_emldflags += --js-library lib.js	# Include our own JS functions
ifneq ($(D),1)
_emldflags += -O3					# Optimization level
else
_emldflags += -g					# Debug and no optimization
endif
ifdef CONFIG_WASM
_emldflags += -s WASM=1
_emldflags += -s TOTAL_MEMORY=67108864 # Bigger initial memory (default is ~16MB)
_emldflags += -s ALLOW_MEMORY_GROWTH=1 # Allow dynamic extension of memory
else
_emldflags += -s WASM=0
endif

EMCFLAGS := $(strip $(_cflags))
EMLDFLAGS := $(strip $(_ldflags) $(_emldflags))


# Object files
## Common to native and JS environments
_objs := cutils.o fs.o fs_cpio.o iomem.o machine.o pci.o riscv_machine.o \
		 serial.o softfp.o temu.o virtio.o
ifdef CONFIG_DECOMP_ZSTD
_objs += decompress_zstd.o
endif
ifdef CONFIG_DECOMP_LZ4
_objs += decompress_lz4.o
endif

## Native environment
_objs_native := $(_objs) host_native.o fs_disk.o
_objs_native += riscv_cpu32.o riscv_cpu64.o
ifdef CONFIG_X86EMU
_objs_native += x86_cpu.o x86_machine.o
endif

OBJS := $(addprefix $(OUT_DIR)/,$(_objs_native))
DEPS := $(patsubst %.o,%.d,$(OBJS))

## JS environment
_objs_js := $(patsubst %.o,%.js.o,$(_objs)) host_js.js.o
_objs_js += riscv_cpu$(CONFIG_RISCV_MAX_XLEN).js.o

OBJS_JS := $(addprefix $(OUT_DIR)/,$(_objs_js))
DEPS_JS := $(patsubst %.o,%.d,$(OBJS_JS))


# Artifacts
## Native environment
ARTF := $(OUT_DIR)/$(OUT)
## JS environment
ARTF_JS := $(OUT_DIR)/$(OUT_JS)


# Compilation rules
## Don't print the commands unless explicitely requested with `make V=1`
ifneq ($(V),1)
Q = @
endif

$(OUT_DIR):
	$(Q)mkdir -p $@

## Native environment
native: $(OUT_DIR) $(ARTF)

$(ARTF): $(OBJS)
	@echo "LD	$(notdir $@)"
	$(Q)$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(OUT_DIR)/riscv_cpu%.o: riscv_cpu.c
	@echo "CC	$(notdir $@)"
	$(Q)$(CC) -c $(CFLAGS) -DMAX_XLEN=$(*) -o $@ $<

$(OUT_DIR)/%.o: %.c
	@echo "CC	$(notdir $@)"
	$(Q)$(CC) -c $(CFLAGS) -o $@ $<

## JS environment
js: $(OUT_DIR) $(ARTF_JS)

$(ARTF_JS): $(OBJS_JS)
	@echo "EMLD	$(notdir $@)"
	$(Q)$(EMCC) $(EMCFLAGS) -o $@ $^ $(EMLDFLAGS)

$(OUT_DIR)/riscv_cpu%.js.o: riscv_cpu.c
	@echo "EMCC	$(notdir $@)"
	$(Q)$(EMCC) -c $(EMCFLAGS) -DMAX_XLEN=$(*) -o $@ $<

$(OUT_DIR)/%.js.o: %.c
	@echo "EMCC	$(notdir $@)"
	$(Q)$(EMCC) -c $(EMCFLAGS) -o $@ $<


## Cleaning rules
clean: clean_native clean_js

distclean:
	@echo "DISTCLEAN"
	$(Q)rm -rf $(OUT_DIR)

clean_native: FORCE
	@echo "CLEAN	native"
	$(Q)rm -f $(ARTF) $(OBJS) $(DEPS)

clean_js: FORCE
	@echo "CLEAN	js"
	$(Q)rm -f $(ARTF_JS) $(patsubst %.js,%.wasm,$(ARTF_JS)) $(OBJS_JS) $(DEPS_JS)

## Rules configuration
FORCE:
.PHONY: FORCE

## Include dependencies
-include $(wildcard *.d)
