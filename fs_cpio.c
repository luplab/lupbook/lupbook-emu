/*
 * Filesystem included in binary as cpio archive
 *
 * Copyright (c) 2016 Fabrice Bellard
 * Copyright (c) 2021 Garrett Hagopian
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <assert.h>
#include <dirent.h>
#if defined(EMSCRIPTEN)
#include <emscripten.h>
#endif
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "cutils.h"
#include "fs.h"
#include "host.h"
#include "list.h"
#include "machine.h"

/***********************************************************/
/* File buffer API */

#define FBUF_END_ALIGN      1024 /* Buffer length must be a multiple of this */
#define FBUF_GROW_FACTOR    2    /* Space to allocate (size*factor) when growing */
#define FBUF_SHRINK_FACTOR  4    /* Ratio of occupancy (1/factor) before shrinking */

typedef struct {
    uint8_t *data;
    size_t allocated_size;
    bool is_cpio_buf;   /* Track if data still part of initial CPIO archive */
} FileBuffer;

static void file_buffer_init(FileBuffer *bs)
{
    bs->data = NULL;
    bs->allocated_size = 0;
    bs->is_cpio_buf = false;
}

static void file_buffer_reset(FileBuffer *bs)
{
    if (bs->allocated_size > 0 && !bs->is_cpio_buf)
        free(bs->data);
    file_buffer_init(bs);
}

static int file_buffer_resize(FileBuffer *bs, size_t fsize_prev, size_t fsize)
{
    uint8_t *new_data;
    size_t new_buf_size;

    /* Determine if current buffer actually needs to be shrunk */
    if ((fsize <= bs->allocated_size)
        /* Current buffer is part of CPIO archive */
        && (bs->is_cpio_buf
            /* Current buffer already has minimal size */
            || bs->allocated_size <= FBUF_END_ALIGN
            /* New size not small enough to trigger shrinking */
            || fsize > bs->allocated_size / FBUF_SHRINK_FACTOR))
        return 0;

    new_buf_size = ALIGN(fsize * FBUF_GROW_FACTOR, FBUF_END_ALIGN);

    if (bs->is_cpio_buf) {
        /* Allocate memory outside of initial CPIO archive */
        bs->is_cpio_buf = false;
        new_data = xmallocz(new_buf_size);
        memcpy(new_data, bs->data, min_int(fsize, fsize_prev));
    } else {
        /* Memory for this file was already dynamic */
        new_data = realloc(bs->data, new_buf_size);
    }

    bs->data = new_data;
    bs->allocated_size = new_buf_size;

    return 0;
}

static void file_buffer_write(FileBuffer *bs, size_t offset, const uint8_t *buf,
                              size_t size)
{
    assert(buf && offset + size <= bs->allocated_size);
    memcpy(bs->data + offset, buf, size);
}

static void file_buffer_read(FileBuffer *bs, size_t offset, uint8_t *buf,
                             size_t size)
{
    assert(buf && offset + size <= bs->allocated_size);
    memcpy(buf, bs->data + offset, size);
}

static void file_buffer_set_region(FileBuffer *bs, uint8_t *buf, size_t size)
{
    assert(buf);
    bs->data = buf;
    bs->allocated_size = size;
    bs->is_cpio_buf = true;
}

/***********************************************************/
/* FSDeviceMem: Type definitions */

typedef enum {
    FT_FIFO = 1,
    FT_CHR = 2,
    FT_DIR = 4,
    FT_BLK = 6,
    FT_REG = 8,
    FT_LNK = 10,
    FT_SOCK = 12,
} FSINodeTypeEnum;

typedef struct {
    struct list_head link;
    uint64_t inode_num; /* inode number */
    int32_t refcount;
    int32_t open_count;
    FSINodeTypeEnum type;
    uint32_t mode;
    uint32_t uid;
    uint32_t gid;
    uint32_t mtime_sec;
    uint32_t ctime_sec;
    uint32_t mtime_nsec;
    uint32_t ctime_nsec;
    union {
        struct {
            size_t size; /* real file size */
            FileBuffer fbuf;
            bool is_fscmd;
        } reg;
        struct {
            struct list_head de_list; /* list of FSDirEntry */
            int size;
        } dir;
        struct {
            uint32_t major;
            uint32_t minor;
        } dev;
        struct {
            char *name;
        } symlink;
    } u;
} FSINode;

typedef struct {
    FSDevice common;

    struct list_head inode_list; /* list of FSINode */
    int64_t inode_count; /* current number of inodes */
    uint64_t inode_limit;
    int64_t fs_blocks;
    uint64_t fs_max_blocks;
    uint64_t inode_num_alloc;
    int block_size_log2;
    uint32_t block_size; /* for stat/statfs */
    FSINode *root_inode;
} FSDeviceMem;

typedef struct {
    struct list_head link;
    FSINode *inode;
    uint8_t mark; /* temporary use only */
    char name[0];
} FSDirEntry;

struct FSFile {
    uint32_t uid;
    FSINode *inode;
    bool is_opened;
    uint32_t open_flags;
};

/***********************************************************/
/* FSDeviceMem: Utility functions */

static int64_t to_blocks(FSDeviceMem *fs, uint64_t size)
{
    return (size + fs->block_size - 1) >> fs->block_size_log2;
}

static inline bool isspace_nolf(int c)
{
    return (c == ' ' || c == '\t');
}

static inline int from_hex(int c)
{
    if (c >= '0' && c <= '9')
        return c - '0';
    else if (c >= 'A' && c <= 'F')
        return c - 'A' + 10;
    else if (c >= 'a' && c <= 'f')
        return c - 'a' + 10;
    else
        return -1;
}

static int parse_fname(char *buf, int buf_size, const char **pp)
{
    const char *p;
    char *q;
    int c, h;

    p = *pp;
    while (isspace_nolf(*p))
        p++;
    if (*p == '\0')
        return -1;
    q = buf;
    if (*p == '"') {
        p++;
        for(;;) {
            c = *p++;
            if (c == '\0' || c == '\n') {
                return -1;
            } else if (c == '\"') {
                break;
            } else if (c == '\\') {
                c = *p++;
                switch(c) {
                    case '\'':
                    case '\"':
                    case '\\':
                        goto add_char;
                    case 'n':
                        c = '\n';
                        goto add_char;
                    case 'r':
                        c = '\r';
                        goto add_char;
                    case 't':
                        c = '\t';
                        goto add_char;
                    case 'x':
                        h = from_hex(*p++);
                        if (h < 0)
                            return -1;
                        c = h << 4;
                        h = from_hex(*p++);
                        if (h < 0)
                            return -1;
                        c |= h;
                        goto add_char;
                    default:
                        return -1;
                }
            } else {
add_char:
                if (q >= buf + buf_size - 1)
                    return -1;
                *q++ = c;
            }
        }
    } else {
        while (!isspace_nolf(*p) && *p != '\0' && *p != '\n') {
            if (q >= buf + buf_size - 1)
                return -1;
            *q++ = *p++;
        }
    }
    *q = '\0';
    *pp = p;
    return 0;
}

/***********************************************************/
/* FSDeviceMem: INode functions */

static void inode_update_mtime(FSDevice *fs, FSINode *n)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    n->mtime_sec = tv.tv_sec;
    n->mtime_nsec = tv.tv_usec * 1000;
}

static FSINode *inode_new(FSDevice *fs1, FSINodeTypeEnum type,
                          uint32_t mode, uint32_t uid, uint32_t gid)
{
    FSDeviceMem *fs = (FSDeviceMem *)fs1;
    FSINode *n;

    n = xmallocz(sizeof(*n));
    n->refcount = 1;
    n->open_count = 0;
    n->inode_num = fs->inode_num_alloc;
    fs->inode_num_alloc++;
    n->type = type;
    n->mode = mode & 0xfff;
    n->uid = uid;
    n->gid = gid;

    switch(type) {
        case FT_REG:
            file_buffer_init(&n->u.reg.fbuf);
            break;
        case FT_DIR:
            init_list_head(&n->u.dir.de_list);
            break;
        default:
            break;
    }

    list_add(&n->link, &fs->inode_list);
    fs->inode_count++;

    inode_update_mtime(fs1, n);
    n->ctime_sec = n->mtime_sec;
    n->ctime_nsec = n->mtime_nsec;

    return n;
}

static void inode_free(FSDevice *fs1, FSINode *n)
{
    FSDeviceMem *fs = (FSDeviceMem *)fs1;

    assert(n->refcount == 0);
    assert(n->open_count == 0);
    switch(n->type) {
        case FT_REG:
            fs->fs_blocks -= to_blocks(fs, n->u.reg.size);
            assert(fs->fs_blocks >= 0);
            file_buffer_reset(&n->u.reg.fbuf);
            break;
        case FT_LNK:
            free(n->u.symlink.name);
            break;
        case FT_DIR:
            assert(list_empty(&n->u.dir.de_list));
            break;
        default:
            break;
    }
    list_del(&n->link);
    free(n);
    fs->inode_count--;
    assert(fs->inode_count >= 0);
}

static FSINode *inode_incref(FSDevice *fs, FSINode *n)
{
    n->refcount++;
    return n;
}

static void inode_decref(FSDevice *fs1, FSINode *n)
{
    assert(n->refcount >= 1);
    if (--n->refcount <= 0 && n->open_count <= 0)
        inode_free(fs1, n);
}

static FSINode *inode_inc_open(FSDevice *fs, FSINode *n)
{
    n->open_count++;
    return n;
}

static void inode_dec_open(FSDevice *fs1, FSINode *n)
{
    assert(n->open_count >= 1);
    if (--n->open_count <= 0 && n->refcount <= 0)
        inode_free(fs1, n);
}

static void inode_to_qid(FSQID *qid, FSINode *n)
{
    if (n->type == FT_DIR)
        qid->type = P9_QTDIR;
    else if (n->type == FT_LNK)
        qid->type = P9_QTSYMLINK;
    else
        qid->type = P9_QTFILE;
    qid->version = 0; /* no caching on client */
    qid->path = n->inode_num;
}

/* warning: the refcount of 'n1' is not incremented by this function */
/* XXX: test FS max size */
static FSDirEntry *inode_dir_add(FSDevice *fs1, FSINode *n, const char *name,
                                 FSINode *n1)
{
    FSDeviceMem *fs = (FSDeviceMem *)fs1;
    FSDirEntry *de;
    int name_len, dirent_size, new_size;
    assert(n->type == FT_DIR);

    name_len = strlen(name);
    de = xmallocz(sizeof(*de) + name_len + 1);
    de->inode = n1;
    memcpy(de->name, name, name_len + 1);
    dirent_size = sizeof(*de) + name_len + 1;
    new_size = n->u.dir.size + dirent_size;
    fs->fs_blocks += to_blocks(fs, new_size) - to_blocks(fs, n->u.dir.size);
    n->u.dir.size = new_size;
    list_add_tail(&de->link, &n->u.dir.de_list);
    return de;
}

static bool inode_is_empty_dir(FSDevice *fs, FSINode *n)
{
    struct list_head *el;
    FSDirEntry *de;

    list_for_each(el, &n->u.dir.de_list) {
        de = list_entry(el, FSDirEntry, link);
        if (strcmp(de->name, ".") != 0 &&
            strcmp(de->name, "..") != 0)
            return false;
    }
    return true;
}

static FSDirEntry *inode_search(FSINode *n, const char *name)
{
    struct list_head *el;
    FSDirEntry *de;

    if (n->type != FT_DIR)
        return NULL;

    list_for_each(el, &n->u.dir.de_list) {
        de = list_entry(el, FSDirEntry, link);
        if (!strcmp(de->name, name))
            return de;
    }
    return NULL;
}

static FSINode *inode_search_path1(FSDevice *fs, FSINode *n, const char *path)
{
    char name[NAME_MAX];
    const char *p, *p1;
    int len;
    FSDirEntry *de;

    p = path;
    if (*p == '/')
        p++;

    if (*p == '\0')
        return n;

    for(;;) {
        p1 = strchr(p, '/');
        if (!p1) {
            len = strlen(p);
        } else {
            len = p1 - p;
            p1++;
        }
        if (len > sizeof(name) - 1)
            return NULL;

        memcpy(name, p, len);
        name[len] = '\0';
        if (n->type != FT_DIR)
            return NULL;

        de = inode_search(n, name);
        if (!de)
            return NULL;

        n = de->inode;
        p = p1;
        if (!p)
            break;
    }
    return n;
}

static FSINode *inode_search_path(FSDevice *fs1, const char *path)
{
    FSDeviceMem *fs = (FSDeviceMem *)fs1;
    if (!fs1)
        return NULL;

    return inode_search_path1(fs1, fs->root_inode, path);
}

static void inode_dirent_delete_no_decref(FSDevice *fs1, FSINode *n,
                                          FSDirEntry *de)
{
    FSDeviceMem *fs = (FSDeviceMem *)fs1;
    int dirent_size, new_size;
    dirent_size = sizeof(*de) + strlen(de->name) + 1;

    new_size = n->u.dir.size - dirent_size;
    fs->fs_blocks += to_blocks(fs, new_size) - to_blocks(fs, n->u.dir.size);
    n->u.dir.size = new_size;
    assert(n->u.dir.size >= 0);
    assert(fs->fs_blocks >= 0);
    list_del(&de->link);
    free(de);
}

static void inode_dirent_delete(FSDevice *fs, FSINode *n, FSDirEntry *de)
{
    FSINode *n1;
    n1 = de->inode;
    inode_dirent_delete_no_decref(fs, n, de);
    inode_decref(fs, n1);
}

static void inode_flush_dir(FSDevice *fs, FSINode *n)
{
    struct list_head *el, *el1;
    FSDirEntry *de;
    list_for_each_safe(el, el1, &n->u.dir.de_list) {
        de = list_entry(el, FSDirEntry, link);
        inode_dirent_delete(fs, n, de);
    }
    assert(n->u.dir.size == 0);
}

/***********************************************************/
/* FSDeviceMem: FSFile functions */

static FSFile *fid_create(FSDevice *fs1, FSINode *n, uint32_t uid)
{
    FSFile *f;

    f = xmallocz(sizeof(*f));
    f->inode = inode_inc_open(fs1, n);
    f->uid = uid;
    return f;
}

/***********************************************************/
/* FSDeviceMem implementation */

static void fs_create_cmd(FSDevice *fs)
{
    FSFile *root_fd;
    FSQID qid;
    FSINode *n;

    assert(!fs->fs_attach(fs, &root_fd, &qid, 0, "", ""));
    assert(!fs->fs_create(fs, &qid, root_fd, FSCMD_NAME, P9_O_RDWR | P9_O_TRUNC,
                          0666, 0));
    n = root_fd->inode;
    n->u.reg.is_fscmd = true;
    fs->fs_delete(fs, root_fd);
}

static void fs_close(FSDevice *fs, FSFile *f)
{
    if (f->is_opened)
        f->is_opened = false;
}

static void fs_delete(FSDevice *fs, FSFile *f)
{
    if (f->is_opened)
        fs_close(fs, f);
    inode_dec_open(fs, f->inode);
    free(f);
}

static void fs_statfs(FSDevice *fs1, FSStatFS *st)
{
    FSDeviceMem *fs = (FSDeviceMem *)fs1;
    st->f_bsize = 1024;
    st->f_blocks = fs->fs_max_blocks <<
        (fs->block_size_log2 - 10);
    st->f_bfree = (fs->fs_max_blocks - fs->fs_blocks) <<
        (fs->block_size_log2 - 10);
    st->f_bavail = st->f_bfree;
    st->f_files = fs->inode_limit;
    st->f_ffree = fs->inode_limit - fs->inode_count;
}

static int fs_attach(FSDevice *fs1, FSFile **pf, FSQID *qid, uint32_t uid,
                     const char *uname, const char *aname)
{
    FSDeviceMem *fs = (FSDeviceMem *)fs1;

    *pf = fid_create(fs1, fs->root_inode, uid);
    inode_to_qid(qid, fs->root_inode);
    return 0;
}

static int fs_walk(FSDevice *fs, FSFile **pf, FSQID *qids,
                   FSFile *f, int count, char **names)
{
    int i;
    FSINode *n;
    FSDirEntry *de;
    n = f->inode;

    for(i = 0; i < count; i++) {
        de = inode_search(n, names[i]);
        if (!de)
            break;
        n = de->inode;
        inode_to_qid(&qids[i], n);
    }
    *pf = fid_create(fs, n, f->uid);
    return i;
}

static int fs_mkdir(FSDevice *fs, FSQID *qid, FSFile *f,
                    const char *name, uint32_t mode, uint32_t gid)
{
    FSINode *n, *n1;

    n = f->inode;
    if (n->type != FT_DIR)
        return -P9_ENOTDIR;
    if (inode_search(n, name))
        return -P9_EEXIST;
    n1 = inode_new(fs, FT_DIR, mode, f->uid, gid);
    inode_dir_add(fs, n1, ".", inode_incref(fs, n1));
    inode_dir_add(fs, n1, "..", inode_incref(fs, n));
    inode_dir_add(fs, n, name, n1);
    inode_to_qid(qid, n1);
    return 0;
}

static int fs_truncate(FSDevice *fs1, FSINode *n, uint64_t size)
{
    FSDeviceMem *fs = (FSDeviceMem *)fs1;
    intptr_t diff, diff_blocks;

    if (n->type != FT_REG)
        return -P9_EINVAL;
    if (size > UINTPTR_MAX)
        return -P9_ENOSPC;
    diff = size - n->u.reg.size;
    if (diff == 0)
        return 0;
    diff_blocks = to_blocks(fs, size) - to_blocks(fs, n->u.reg.size);

    if (file_buffer_resize(&n->u.reg.fbuf, n->u.reg.size, size) != 0)
        return -P9_ENOSPC;

    fs->fs_blocks += diff_blocks;
    assert(fs->fs_blocks >= 0);
    n->u.reg.size = size;
    return 0;
}

/* return < 0 if error, 0 if OK, 1 if asynchronous completion */
/* XXX: we don't support several simultaneous asynchronous open on the
   same inode */
static int fs_open(FSDevice *fs1, FSQID *qid, FSFile *f, uint32_t flags,
                   FSOpenCompletionFunc *cb, void *opaque)
{
    FSINode *n = f->inode;

    fs_close(fs1, f);

    if (flags & P9_O_DIRECTORY) {
        if (n->type != FT_DIR)
            return -P9_ENOTDIR;
    } else {
        if (n->type != FT_REG && n->type != FT_DIR)
            return -P9_EINVAL; /* XXX */
    }
    f->open_flags = flags;
    if (n->type == FT_REG &&
        (flags & P9_O_TRUNC) && (flags & P9_O_NOACCESS) != P9_O_RDONLY)
        fs_truncate(fs1, n, 0);

    f->is_opened = true;
    inode_to_qid(qid, n);
    return 0;
}

static int fs_create(FSDevice *fs, FSQID *qid, FSFile *f, const char *name,
                     uint32_t flags, uint32_t mode, uint32_t gid)
{
    FSINode *n1, *n = f->inode;

    if (n->type != FT_DIR)
        return -P9_ENOTDIR;
    if (inode_search(n, name)) {
        /* XXX: support it, but Linux does not seem to use this case */
        return -P9_EEXIST;
    } else {
        fs_close(fs, f);

        n1 = inode_new(fs, FT_REG, mode, f->uid, gid);
        inode_dir_add(fs, n, name, n1);

        inode_dec_open(fs, f->inode);
        f->inode = inode_inc_open(fs, n1);
        f->is_opened = true;
        f->open_flags = flags;
        inode_to_qid(qid, n1);
        return 0;
    }
}

static int fs_readdir(FSDevice *fs, FSFile *f, uint64_t offset1,
                      uint8_t *buf, int count)
{
    FSINode *n1, *n = f->inode;
    int len, pos, name_len, type;
    struct list_head *el;
    FSDirEntry *de;
    uint64_t offset;

    if (!f->is_opened || n->type != FT_DIR)
        return -P9_EPROTO;

    el = n->u.dir.de_list.next;
    offset = 0;
    while (offset < offset1) {
        if (el == &n->u.dir.de_list)
            return 0; /* no more entries */
        offset++;
        el = el->next;
    }

    pos = 0;
    for(;;) {
        if (el == &n->u.dir.de_list)
            break;
        de = list_entry(el, FSDirEntry, link);
        name_len = strlen(de->name);
        len = 13 + 8 + 1 + 2 + name_len;
        if ((pos + len) > count)
            break;
        offset++;
        n1 = de->inode;
        if (n1->type == FT_DIR)
            type = P9_QTDIR;
        else if (n1->type == FT_LNK)
            type = P9_QTSYMLINK;
        else
            type = P9_QTFILE;
        buf[pos++] = type;
        put_le32(buf + pos, 0); /* version */
        pos += 4;
        put_le64(buf + pos, n1->inode_num);
        pos += 8;
        put_le64(buf + pos, offset);
        pos += 8;
        buf[pos++] = n1->type;
        put_le16(buf + pos, name_len);
        pos += 2;
        memcpy(buf + pos, de->name, name_len);
        pos += name_len;
        el = el->next;
    }
    return pos;
}

static int fs_read(FSDevice *fs, FSFile *f, uint64_t offset,
                   uint8_t *buf, int count)
{
    FSINode *n = f->inode;
    uint64_t count1;

    if (!f->is_opened)
        return -P9_EPROTO;

    if (n->type != FT_REG)
        return -P9_EIO;

    if ((f->open_flags & P9_O_NOACCESS) == P9_O_WRONLY)
        return -P9_EIO;

    if (offset >= n->u.reg.size)
        return 0;
    count1 = n->u.reg.size - offset;
    if (count1 < count)
        count = count1;
    file_buffer_read(&n->u.reg.fbuf, offset, buf, count);
    return count;
}

#ifndef EMSCRIPTEN
/* Otherwise, provided in lib.js */
int fs_export_file(const char *fpathname, int rc, const uint8_t *buf, int size)
{
    fprintf(stderr, "File export not supported in non-JS environment\n");
    return -1;
}
#endif

static int fs_cmd_export_file(FSDevice *fs, const char *p)
{
    char export_path[PATH_MAX];
    FSINode *n;

    if (parse_fname(export_path, sizeof(export_path), &p) < 0)
        return fs_export_file(export_path, -P9_EIO, NULL, 0);

    n = inode_search_path(fs, export_path);
    if (!n)
        return fs_export_file(export_path, -P9_ENOENT, NULL, 0);

    if (n->type != FT_REG)
        return fs_export_file(export_path, -P9_EIO, NULL, 0);

    return fs_export_file(export_path, 0, n->u.reg.fbuf.data, n->u.reg.size);
}

static int fs_cmd_write(FSDevice *fs, FSFile *f, uint64_t offset,
                        const uint8_t *buf, int buf_len)
{
    char *buf1;
    const char *p;
    char cmd[64];
    int err;

    /* Transform into a string */
    buf1 = xmalloc(buf_len + 1);
    memcpy(buf1, buf, buf_len);
    buf1[buf_len] = '\0';

    err = 0;
    p = buf1;

    if (parse_fname(cmd, sizeof(cmd), &p) < 0)
        err = -P9_EIO;
    else if (!strcmp(cmd, "export_file"))
        err = fs_cmd_export_file(fs, p);
    else
        err = -P9_EIO;

    free(buf1);
    if (err == 0)
        return buf_len;
    else
        return err;
}

static int fs_write(FSDevice *fs1, FSFile *f, uint64_t offset,
                    const uint8_t *buf, int count)
{
    FSINode *n = f->inode;
    uint64_t end;
    int err;

    if (!f->is_opened)
        return -P9_EPROTO;
    if (n->type != FT_REG)
        return -P9_EIO;
    if ((f->open_flags & P9_O_NOACCESS) == P9_O_RDONLY)
        return -P9_EIO;
    if (count == 0)
        return 0;

    if (n->u.reg.is_fscmd)
        return fs_cmd_write(fs1, f, offset, buf, count);

    end = offset + count;
    if (end > n->u.reg.size) {
        err = fs_truncate(fs1, n, end);
        if (err)
            return err;
    }

    inode_update_mtime(fs1, n);

    file_buffer_write(&n->u.reg.fbuf, offset, buf, count);

    return count;
}

static int fs_stat(FSDevice *fs1, FSFile *f, FSStat *st)
{
    FSDeviceMem *fs = (FSDeviceMem *)fs1;
    FSINode *n = f->inode;

    inode_to_qid(&st->qid, n);
    st->st_mode = n->mode | (n->type << 12);
    st->st_uid = n->uid;
    st->st_gid = n->gid;
    st->st_nlink = n->refcount;
    if (n->type == FT_BLK || n->type == FT_CHR)
        /* XXX: check */
        st->st_rdev = (n->u.dev.major << 8) | n->u.dev.minor;
    else
        st->st_rdev = 0;

    st->st_blksize = fs->block_size;
    if (n->type == FT_REG)
        st->st_size = n->u.reg.size;
    else if (n->type == FT_LNK)
        st->st_size = strlen(n->u.symlink.name);
    else if (n->type == FT_DIR)
        st->st_size = n->u.dir.size;
    else
        st->st_size = 0;
    /* In 512 byte blocks */
    st->st_blocks = to_blocks(fs, st->st_size) << (fs->block_size_log2 - 9);

    /* Note: atime is not supported */
    st->st_atime_sec = n->mtime_sec;
    st->st_atime_nsec = n->mtime_nsec;
    st->st_mtime_sec = n->mtime_sec;
    st->st_mtime_nsec = n->mtime_nsec;
    st->st_ctime_sec = n->ctime_sec;
    st->st_ctime_nsec = n->ctime_nsec;
    return 0;
}

static int fs_setattr(FSDevice *fs1, FSFile *f, uint32_t mask,
                      uint32_t mode, uint32_t uid, uint32_t gid,
                      uint64_t size, uint64_t atime_sec, uint64_t atime_nsec,
                      uint64_t mtime_sec, uint64_t mtime_nsec)
{
    FSINode *n = f->inode;
    int ret;

    if (mask & P9_SETATTR_MODE)
        n->mode = mode;
    if (mask & P9_SETATTR_UID)
        n->uid = uid;
    if (mask & P9_SETATTR_GID)
        n->gid = gid;

    if (mask & P9_SETATTR_SIZE) {
        ret = fs_truncate(fs1, n, size);
        if (ret)
            return ret;
    }
    if (mask & P9_SETATTR_MTIME) {
        if (mask & P9_SETATTR_MTIME_SET) {
            n->mtime_sec = mtime_sec;
            n->mtime_nsec = mtime_nsec;
        } else {
            inode_update_mtime(fs1, n);
        }
    }
    if (mask & P9_SETATTR_CTIME) {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        n->ctime_sec = tv.tv_sec;
        n->ctime_nsec = tv.tv_usec * 1000;
    }
    return 0;
}

static int fs_link(FSDevice *fs, FSFile *df, FSFile *f, const char *name)
{
    FSINode *n = df->inode;

    if (f->inode->type == FT_DIR)
        return -P9_EPERM;
    if (inode_search(n, name))
        return -P9_EEXIST;
    inode_dir_add(fs, n, name, inode_incref(fs, f->inode));
    return 0;
}

static int fs_symlink(FSDevice *fs, FSQID *qid, FSFile *f,
                      const char *name, const char *symgt, uint32_t gid)
{
    FSINode *n1, *n = f->inode;

    if (inode_search(n, name))
        return -P9_EEXIST;

    n1 = inode_new(fs, FT_LNK, 0777, f->uid, gid);
    n1->u.symlink.name = strdup(symgt);
    inode_dir_add(fs, n, name, n1);
    inode_to_qid(qid, n1);
    return 0;
}

static int fs_mknod(FSDevice *fs, FSQID *qid,
                    FSFile *f, const char *name, uint32_t mode, uint32_t major,
                    uint32_t minor, uint32_t gid)
{
    int type;
    FSINode *n1, *n = f->inode;

    type = (mode & P9_S_IFMT) >> 12;
    /* XXX: add FT_DIR support */
    if (type != FT_FIFO && type != FT_CHR && type != FT_BLK &&
        type != FT_REG && type != FT_SOCK)
        return -P9_EINVAL;
    if (inode_search(n, name))
        return -P9_EEXIST;
    n1 = inode_new(fs, type, mode, f->uid, gid);
    if (type == FT_CHR || type == FT_BLK) {
        n1->u.dev.major = major;
        n1->u.dev.minor = minor;
    }
    inode_dir_add(fs, n, name, n1);
    inode_to_qid(qid, n1);
    return 0;
}

static int fs_readlink(FSDevice *fs, char *buf, int buf_size, FSFile *f)
{
    FSINode *n = f->inode;
    int len;
    if (n->type != FT_LNK)
        return -P9_EIO;
    len = min_int(strlen(n->u.symlink.name), buf_size - 1);
    memcpy(buf, n->u.symlink.name, len);
    buf[len] = '\0';
    return 0;
}

static int fs_renameat(FSDevice *fs, FSFile *f, const char *name,
                       FSFile *new_f, const char *new_name)
{
    FSDirEntry *de, *de1;
    FSINode *n1;

    de = inode_search(f->inode, name);
    if (!de)
        return -P9_ENOENT;
    de1 = inode_search(new_f->inode, new_name);
    n1 = NULL;
    if (de1) {
        n1 = de1->inode;
        if (n1->type == FT_DIR)
            return -P9_EEXIST; /* XXX: handle the case */
        inode_dirent_delete_no_decref(fs, new_f->inode, de1);
    }
    inode_dir_add(fs, new_f->inode, new_name, inode_incref(fs, de->inode));
    inode_dirent_delete(fs, f->inode, de);
    if (n1)
        inode_decref(fs, n1);
    return 0;
}

static int fs_unlinkat(FSDevice *fs, FSFile *f, const char *name)
{
    FSDirEntry *de;
    FSINode *n;

    if (!strcmp(name, ".") || !strcmp(name, ".."))
        return -P9_ENOENT;
    de = inode_search(f->inode, name);
    if (!de)
        return -P9_ENOENT;
    n = de->inode;
    if (n->type == FT_DIR) {
        if (!inode_is_empty_dir(fs, n))
            return -P9_ENOTEMPTY;
        inode_flush_dir(fs, n);
    }
    inode_dirent_delete(fs, f->inode, de);
    return 0;
}

static int fs_lock(FSDevice *fs, FSFile *f, const FSLock *lock)
{
    FSINode *n = f->inode;
    if (!f->is_opened)
        return -P9_EPROTO;
    if (n->type != FT_REG)
        return -P9_EIO;
    /* XXX: implement it */
    return P9_LOCK_SUCCESS;
}

static int fs_getlock(FSDevice *fs, FSFile *f, FSLock *lock)
{
    FSINode *n = f->inode;
    if (!f->is_opened)
        return -P9_EPROTO;
    if (n->type != FT_REG)
        return -P9_EIO;
    /* XXX: implement it */
    return 0;
}

/* XXX: only used with file lists, so not all the data is released */
static void _fs_end(FSDevice *fs1)
{
    FSDeviceMem *fs = (FSDeviceMem *)fs1;
    struct list_head *el, *el1, *el2, *el3;
    FSINode *n;
    FSDirEntry *de;

    list_for_each_safe(el, el1, &fs->inode_list) {
        n = list_entry(el, FSINode, link);
        n->refcount = 0;
        if (n->type == FT_DIR) {
            list_for_each_safe(el2, el3, &n->u.dir.de_list) {
                de = list_entry(el2, FSDirEntry, link);
                list_del(&de->link);
                free(de);
            }
            init_list_head(&n->u.dir.de_list);
        }
        inode_free(fs1, n);
    }
}

/***********************************************************/
/* FScmd - support host-guest interactions */

static FSDevice *fs_cpio_fs;

/* Exported to JS */
int fs_import_file(const char *fpath, const char *fname, uint8_t *buf,
                    int buf_len)
{
    FSDevice *fs;
    FSFile *fd, *root_fd;
    FSQID qid;
    int ret = -1;

    if (fpath == NULL || fname == NULL) {
        return ret;
    }

    fs = fs_cpio_fs;
    if (!fs) {
        free(buf);
        return ret;
    }

    assert(!fs->fs_attach(fs, &root_fd, &qid, 1000, "", ""));

    fd = fs_walk_path(fs, root_fd, fpath);
    if (fd) {
        fs_unlinkat(fs, fd, fname);
        if ((ret = fs->fs_create(fs, &qid, fd, fname, P9_O_RDWR | P9_O_TRUNC,
                          0600, 0)) >= 0) {
            ret = fs->fs_write(fs, fd, 0, buf, buf_len);
        }
        fs->fs_delete(fs, fd);
    }

    if (root_fd)
        fs->fs_delete(fs, root_fd);
    free(buf);

    return ret;
}

/***********************************************************/
/* CPIO file parsing */

#define CPIO_MAGIC_OLD 070707
#define CPIO_MAGIC_PORTABLE "\x00\x07\x00\x07\x00\x07"
#define CPIO_MAGIC_ASCII_NEW "070701"
#define CPIO_MAGIC_TRAILER "TRAILER!!!"

#define PATH_SEPARATOR '/'

typedef struct {
    unsigned short c_magic;
    unsigned short c_dev;
    unsigned short c_ino;
    unsigned short c_mode;
    unsigned short c_uid;
    unsigned short c_gid;
    unsigned short c_nlink;
    unsigned short c_rdev;
    unsigned short c_mtime[2];
    unsigned short c_namesize;
    unsigned short c_filesize[2];
} cpio_header_old;

typedef struct {
    char c_magic[6];
    char c_ino[8];
    char c_mode[8];
    char c_uid[8];
    char c_gid[8];
    char c_nlink[8];
    char c_mtime[8];
    char c_filesize[8];
    char c_devmajor[8];
    char c_devminor[8];
    char c_rdevmajor[8];
    char c_rdevminor[8];
    char c_namesize[8];
    char c_check[8];
} cpio_header_ascii_new;

typedef struct {
    cpio_header_old     hdr;
    char               *fname;
    char               *fdata;
    size_t              fdata_len;
    char               *next_hdr;
} cpio_entry;

typedef struct {
    struct list_head link;
    uint64_t c_ino;
    FSINode *inode;
} cpio_linked_inode;

typedef int (*cpio_iter_fn)(char *, char *, cpio_entry *);

static int cpio_iter_common(char *pos, char *end, cpio_entry *cur)
{
    if (!strncmp(cur->fname, CPIO_MAGIC_TRAILER, cur->hdr.c_namesize))
        cur->next_hdr = NULL;

    return 0;
}

static int cpio_iter_old(char *pos, char *last, cpio_entry *cur)
{
    if (pos + sizeof(cpio_header_old) >= last)
        return -1;

    memcpy(&cur->hdr, pos, sizeof(cpio_header_old));
    if (cur->hdr.c_magic != CPIO_MAGIC_OLD)
        return -1;

    cur->fname = pos + sizeof(cpio_header_old);
    cur->fdata = cur->fname + cur->hdr.c_namesize +
        ((cur->hdr.c_namesize % 2 == 1) ? 1 : 0);
    cur->fdata_len = (cur->hdr.c_filesize[0] << (sizeof(unsigned short) * 8)) +
        cur->hdr.c_filesize[1];
    cur->next_hdr = cur->fdata + cur->fdata_len +
        ((cur->hdr.c_filesize[1] % 2 == 1) ? 1 : 0);

    return cpio_iter_common(pos, last, cur);
}

static int cpio_iter_ascii_new(char *pos, char *last, cpio_entry *cur)
{
    char buf[9];
    buf[8] = '\0';

    if (pos + sizeof(cpio_iter_ascii_new) >= last ||
        strncmp(pos, CPIO_MAGIC_ASCII_NEW, 6))
        return -1;

    cpio_header_ascii_new *hdr_new = (cpio_header_ascii_new *)pos;

    memcpy(buf, hdr_new->c_ino, 8);
    cur->hdr.c_ino = (unsigned short)strtoul(buf, NULL, 16);
    memcpy(buf, hdr_new->c_mode, 8);
    cur->hdr.c_mode = (unsigned short)strtoul(buf, NULL, 16);
    memcpy(buf, hdr_new->c_uid, 8);
    cur->hdr.c_uid = (unsigned short)strtoul(buf, NULL, 16);
    memcpy(buf, hdr_new->c_gid, 8);
    cur->hdr.c_gid = (unsigned short)strtoul(buf, NULL, 16);
    memcpy(buf, hdr_new->c_nlink, 8);
    cur->hdr.c_nlink = (unsigned short)strtoul(buf, NULL, 16);

    memcpy(buf, hdr_new->c_rdevmajor, 8);
    cur->hdr.c_rdev += (uint8_t)strtoul(buf, NULL, 16) << 8;
    memcpy(buf, hdr_new->c_rdevminor, 8);
    cur->hdr.c_rdev += (uint8_t)strtoul(buf, NULL, 16);

    memcpy(buf, hdr_new->c_mtime, 8);
    unsigned long mtime = strtoul(buf, NULL, 16);
    cur->hdr.c_mtime[0] = mtime >> 16;
    cur->hdr.c_mtime[1] = mtime & 0xFFFF;

    memcpy(buf, hdr_new->c_namesize, 8);
    cur->hdr.c_namesize = (unsigned short)strtoul(buf, NULL, 16);

    memcpy(buf, hdr_new->c_filesize, 8);
    unsigned long c_filesize = strtoul(buf, NULL, 16);
    cur->hdr.c_filesize[0] = c_filesize >> 16;
    cur->hdr.c_filesize[1] = c_filesize & 0xFFFF;

    cur->fname = pos + sizeof(cpio_header_ascii_new);
    cur->fdata = cur->fname + cur->hdr.c_namesize +
        ((4 - ((cur->hdr.c_namesize + sizeof(cpio_header_ascii_new)) & 0x3))
         & 0x3);
    cur->fdata_len = (cur->hdr.c_filesize[0] << (sizeof(unsigned short) * 8)) +
        cur->hdr.c_filesize[1];
    cur->next_hdr = cur->fdata + cur->fdata_len +
        ((4 - (cur->hdr.c_filesize[1] & 0x3)) & 0x3);

    return cpio_iter_common(pos, last, cur);
}

static cpio_iter_fn cpio_check_format(char *buf, size_t len)
{
    if (len >= sizeof(cpio_header_old) &&
        *(uint16_t *)buf == CPIO_MAGIC_OLD)
        /* Old format */
        return cpio_iter_old;

    if (len >= sizeof(cpio_header_ascii_new) &&
        !strncmp(buf, CPIO_MAGIC_ASCII_NEW, 6))
        /* New ascii format */
        return cpio_iter_ascii_new;

    if (len >= sizeof(CPIO_MAGIC_PORTABLE) &&
        !memcmp(buf, CPIO_MAGIC_PORTABLE, 6))
        /* Portable ascii format */
        return NULL;

    return NULL;
}

/* If FSINode is passed, merge its contents with those of the cpio entry */
/* Otherwise, allocate a new FSINode */
static FSINode *cpio_to_inode(FSDevice *fs1, cpio_entry *ent, FSINode *n)
{
    FSINodeTypeEnum ntype;

    switch (ent->hdr.c_mode & 0170000) {
        case 0140000: ntype = FT_SOCK ; break;
        case 0120000: ntype = FT_LNK  ; break;
        case 0100000: ntype = FT_REG  ; break;
        case 0060000: ntype = FT_BLK  ; break;
        case 0040000: ntype = FT_DIR  ; break;
        case 0020000: ntype = FT_CHR  ; break;
        case 0010000: ntype = FT_FIFO ; break;
        default: ntype = 0;
    }

    if (!n) {
        n = inode_new(fs1, ntype, ent->hdr.c_mode, ent->hdr.c_uid,
                      ent->hdr.c_gid);
        n->ctime_sec = (ent->hdr.c_mtime[0] << (sizeof(short) * 8)) +
                        ent->hdr.c_mtime[1];
        n->mtime_sec = n->ctime_sec;
    }

    /* check for empty file or hard-link for which content will be added later */
    if (ntype == FT_REG && !(ent->hdr.c_filesize[0] | ent->hdr.c_filesize[1]))
        return n;

    if (ntype == FT_LNK) {
        n->u.symlink.name = xmalloc(ent->fdata_len + 1);
        memcpy(n->u.symlink.name, ent->fdata, ent->fdata_len);
        n->u.symlink.name[ent->fdata_len] = '\0';
    } else if (ntype == FT_REG) {
        n->u.reg.size = (ent->hdr.c_filesize[0] << (sizeof(short) * 8)) +
                         ent->hdr.c_filesize[1];
        file_buffer_set_region(&n->u.reg.fbuf, (uint8_t *)ent->fdata,
                               n->u.reg.size);
    } else if (ntype != FT_DIR) {
        n->u.dev.major = (ent->hdr.c_rdev & 0xFF00) >> 8;
        n->u.dev.minor = ent->hdr.c_rdev & 0x00FF;
    }

    return n;
}

static void cpio_init(FSDevice *fs1, cpio_iter_fn get_next_hdr,
                      char *buf, size_t len)
{
    FSDeviceMem *fs = (FSDeviceMem *)fs1;
    cpio_entry cpio_cur;
    char *path_end, *new_fname;
    FSINode *new, *parent;

    struct list_head *el, *el1;
    struct list_head hl_list;
    cpio_linked_inode *hl;

    char *cpio_pos = buf;
    char *cpio_last = buf + len;

    init_list_head(&hl_list);

    while (cpio_pos < cpio_last) {
        /* Get node details */
        if (get_next_hdr(cpio_pos, cpio_last, &cpio_cur) != 0)
            die("Corrupt CPIO archive");

        if (!cpio_cur.next_hdr)
            break;
        cpio_pos = cpio_cur.next_hdr;

        if (cpio_cur.hdr.c_namesize == 2 && *cpio_cur.fname == '.')
            continue;

        new = NULL;
        if (cpio_cur.hdr.c_nlink > 1 &&
            (cpio_cur.hdr.c_mode & 0170000) != 0040000) { /* not a directory */
            list_for_each(el, &hl_list) {
                hl = list_entry(el, cpio_linked_inode, link);
                if (hl->c_ino == cpio_cur.hdr.c_ino) {
                    new = cpio_to_inode(fs1, &cpio_cur, hl->inode);
                    break;
                }
            }
        }

        if (new == NULL) {
            new = cpio_to_inode(fs1, &cpio_cur, NULL);
            if (cpio_cur.hdr.c_nlink > 1 && new->type != FT_DIR) {
                hl = xmallocz(sizeof(*hl));
                hl->c_ino = cpio_cur.hdr.c_ino;
                hl->inode = new;
                list_add(&hl->link, &hl_list);
            }
        }

        /* Find node's parent in inode tree */
        parent = NULL;
        path_end = strrchr(cpio_cur.fname, PATH_SEPARATOR);
        if (!path_end) {
            parent = fs->root_inode;
            new_fname = cpio_cur.fname;
        } else if (*(path_end + 1) != '\0') { /* Don't support trailing '/' */
            *path_end = '\0';
            new_fname = path_end + 1;
            parent = inode_search_path(fs1, cpio_cur.fname);
            *path_end = PATH_SEPARATOR;
        }

        if (!parent)
            continue;

        if (new->type == FT_DIR) {
            inode_dir_add(fs1, new, ".", inode_incref(fs1, new));
            inode_dir_add(fs1, new, "..", inode_incref(fs1, parent));
        }

        inode_dir_add(fs1, parent, new_fname, new);
    }

    list_for_each_safe(el, el1, &hl_list) {
        free(el);
    }
}

/***********************************************************/
/* Entry point */

#define FS_BLOCK_SIZE_LOG2 12 /* 4KiB block size */

FSDevice *fs_cpio_init(const char *cpio_filename)
{
    FSDeviceMem *fs;
    FSDevice *fs1;
    FSINode *new;
    char *cpio_buf;
    int cpio_len;

    cpio_len = load_file((uint8_t **)&cpio_buf,  cpio_filename);

    /* Check cpio format */
    cpio_iter_fn get_next_hdr = cpio_check_format(cpio_buf, cpio_len);
    if (!get_next_hdr) {
        free(cpio_buf);
        return NULL;
    }

    fs = xmallocz(sizeof(*fs));
    fs1 = (FSDevice *)fs;

    if (!fs_cpio_fs)
        fs_cpio_fs = (FSDevice *)fs;

    /* Initialize common file system functions */
    fs->common.fs_end = _fs_end;
    fs->common.fs_delete = fs_delete;
    fs->common.fs_statfs = fs_statfs;
    fs->common.fs_attach = fs_attach;
    fs->common.fs_walk = fs_walk;
    fs->common.fs_mkdir = fs_mkdir;
    fs->common.fs_open = fs_open;
    fs->common.fs_create = fs_create;
    fs->common.fs_stat = fs_stat;
    fs->common.fs_setattr = fs_setattr;
    fs->common.fs_close = fs_close;
    fs->common.fs_readdir = fs_readdir;
    fs->common.fs_read = fs_read;
    fs->common.fs_write = fs_write;
    fs->common.fs_link = fs_link;
    fs->common.fs_symlink = fs_symlink;
    fs->common.fs_mknod = fs_mknod;
    fs->common.fs_readlink = fs_readlink;
    fs->common.fs_renameat = fs_renameat;
    fs->common.fs_unlinkat = fs_unlinkat;
    fs->common.fs_lock = fs_lock;
    fs->common.fs_getlock = fs_getlock;

    /* Initialize in-memory specific fields */
    init_list_head(&fs->inode_list);
    fs->inode_num_alloc = 1;
    fs->block_size_log2 = FS_BLOCK_SIZE_LOG2;
    fs->block_size = 1 << fs->block_size_log2;
    fs->inode_limit = 1 << 20; /* arbitrary */
    fs->fs_max_blocks = 1 << (30 - fs->block_size_log2); /* arbitrary */

    /* Create the root inode */
    new = inode_new(fs1, FT_DIR, 0777, 0, 0);
    inode_dir_add(fs1, new, ".", inode_incref(fs1, new));
    inode_dir_add(fs1, new, "..", inode_incref(fs1, new));
    fs->root_inode = new;
    fs_create_cmd(fs1);

    /* Parse cpio contents */
    cpio_init(fs1, get_next_hdr, cpio_buf, cpio_len);

    return (FSDevice *)fs;
}
