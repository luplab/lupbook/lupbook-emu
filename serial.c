/*
 * Serial device
 *
 * Copyright (c) 2016-2017 Fabrice Bellard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "cutils.h"
#include "iomem.h"
#include "serial.h"

/***********************************************************/
/* serial port emulation */

#define UART_LCR_DLAB   0x80    /* Divisor latch access bit */

#define UART_IER_MSI    0x08    /* Enable Modem status interrupt */
#define UART_IER_RLSI   0x04    /* Enable receiver line status interrupt */
#define UART_IER_THRI   0x02    /* Enable Transmitter holding register int. */
#define UART_IER_RDI    0x01    /* Enable receiver data interrupt */

#define UART_IIR_NO_INT 0x01    /* No interrupts pending */
#define UART_IIR_ID     0x06    /* Mask for the interrupt ID */

#define UART_IIR_MSI    0x00    /* Modem status interrupt */
#define UART_IIR_THRI   0x02    /* Transmitter holding register empty */
#define UART_IIR_RDI    0x04    /* Receiver data interrupt */
#define UART_IIR_RLSI   0x06    /* Receiver line status interrupt */
#define UART_IIR_FE     0xC0    /* Fifo enabled */

#define UART_LSR_TEMT   0x40    /* Transmitter empty */
#define UART_LSR_THRE   0x20    /* Transmit-hold-register empty */
#define UART_LSR_BI     0x10    /* Break interrupt indicator */
#define UART_LSR_FE     0x08    /* Frame error indicator */
#define UART_LSR_PE     0x04    /* Parity error indicator */
#define UART_LSR_OE     0x02    /* Overrun error indicator */
#define UART_LSR_DR     0x01    /* Receiver data ready */

#define UART_FCR_XFR    0x04    /* XMIT Fifo Reset */
#define UART_FCR_RFR    0x02    /* RCVR Fifo Reset */
#define UART_FCR_FE     0x01    /* FIFO Enable */

#define UART_FIFO_LENGTH    16      /* 16550A Fifo Length */

struct SerialState {
    uint8_t divider;
    uint8_t rbr; /* receive register */
    uint8_t ier;
    uint8_t iir; /* read only */
    uint8_t lcr;
    uint8_t mcr;
    uint8_t lsr; /* read only */
    uint8_t msr;
    uint8_t scr;
    uint8_t fcr;
    IRQSignal *irq;
    void (*write_func)(void *opaque, const uint8_t *buf, int buf_len);
    void *opaque;
};

static void serial_write(void *opaque, uint32_t offset,
                         uint32_t val, int size_log2);
static uint32_t serial_read(void *opaque, uint32_t offset, int size_log2);

SerialState *serial_init(PhysMemoryMap *port_map, int addr,
                         IRQSignal *irq,
                         void (*write_func)(void *opaque, const uint8_t *buf, int buf_len), void *opaque)
{
    SerialState *s;
    s = xmallocz(sizeof(*s));

    /* all 8 bit registers */
    s->divider = 0;
    s->rbr = 0; /* receive register */
    s->ier = 0;
    s->iir = UART_IIR_NO_INT; /* read only */
    s->lcr = 0;
    s->mcr = 0;
    s->lsr = UART_LSR_TEMT | UART_LSR_THRE; /* read only */
    s->msr = 0;
    s->scr = 0;
    s->fcr = 0;

    s->irq = irq;
    s->write_func = write_func;
    s->opaque = opaque;

    cpu_register_device(port_map, addr, 8, s, serial_read, serial_write,
                        DEVIO_SIZE8);
    return s;
}

static void serial_update_irq(SerialState *s)
{
    if ((s->lsr & UART_LSR_DR) && (s->ier & UART_IER_RDI)) {
        s->iir = UART_IIR_RDI;
    } else if ((s->lsr & UART_LSR_THRE) && (s->ier & UART_IER_THRI)) {
        s->iir = UART_IIR_THRI;
    } else {
        s->iir = UART_IIR_NO_INT;
    }
    if (s->iir != UART_IIR_NO_INT) {
        set_irq(s->irq, 1);
    } else {
        set_irq(s->irq, 0);
    }
}

#if 0
/* send remainining chars in fifo */
Serial.prototype.write_tx_fifo = function()
{
    if (s->tx_fifo != "") {
        s->write_func(s->tx_fifo);
        s->tx_fifo = "";

        s->lsr |= UART_LSR_THRE;
        s->lsr |= UART_LSR_TEMT;
        s->update_irq();
    }
}
#endif

static void serial_write(void *opaque, uint32_t offset,
                         uint32_t val, int size_log2)
{
    SerialState *s = opaque;
    int addr;

    addr = offset & 7;
    switch(addr) {
        default:
        case 0:
            if (s->lcr & UART_LCR_DLAB) {
                s->divider = (s->divider & 0xff00) | val;
            } else {
#if 0
                if (s->fcr & UART_FCR_FE) {
                    s->tx_fifo += String.fromCharCode(val);
                    s->lsr &= ~UART_LSR_THRE;
                    serial_update_irq(s);
                    if (s->tx_fifo.length >= UART_FIFO_LENGTH) {
                        /* write to the terminal */
                        s->write_tx_fifo();
                    }
                } else
#endif
                {
                    uint8_t ch;
                    s->lsr &= ~UART_LSR_THRE;
                    serial_update_irq(s);

                    /* write to the terminal */
                    ch = val;
                    s->write_func(s->opaque, &ch, 1);
                    s->lsr |= UART_LSR_THRE;
                    s->lsr |= UART_LSR_TEMT;
                    serial_update_irq(s);
                }
            }
            break;
        case 1:
            if (s->lcr & UART_LCR_DLAB) {
                s->divider = (s->divider & 0x00ff) | (val << 8);
            } else {
                s->ier = val;
                serial_update_irq(s);
            }
            break;
        case 2:
#if 0
            if ((s->fcr ^ val) & UART_FCR_FE) {
                /* clear fifos */
                val |= UART_FCR_XFR | UART_FCR_RFR;
            }
            if (val & UART_FCR_XFR)
                s->tx_fifo = "";
            if (val & UART_FCR_RFR)
                s->rx_fifo = "";
            s->fcr = val & UART_FCR_FE;
#endif
            break;
        case 3:
            s->lcr = val;
            break;
        case 4:
            s->mcr = val;
            break;
        case 5:
            break;
        case 6:
            s->msr = val;
            break;
        case 7:
            s->scr = val;
            break;
    }
}

static uint32_t serial_read(void *opaque, uint32_t offset, int size_log2)
{
    SerialState *s = opaque;
    int ret, addr;

    addr = offset & 7;
    switch(addr) {
        default:
        case 0:
            if (s->lcr & UART_LCR_DLAB) {
                ret = s->divider & 0xff;
            } else {
                ret = s->rbr;
                s->lsr &= ~(UART_LSR_DR | UART_LSR_BI);
                serial_update_irq(s);
#if 0
                /* try to receive next chars */
                s->send_char_from_fifo();
#endif
            }
            break;
        case 1:
            if (s->lcr & UART_LCR_DLAB) {
                ret = (s->divider >> 8) & 0xff;
            } else {
                ret = s->ier;
            }
            break;
        case 2:
            ret = s->iir;
            if (s->fcr & UART_FCR_FE)
                ret |= UART_IIR_FE;
            break;
        case 3:
            ret = s->lcr;
            break;
        case 4:
            ret = s->mcr;
            break;
        case 5:
            ret = s->lsr;
            break;
        case 6:
            ret = s->msr;
            break;
        case 7:
            ret = s->scr;
            break;
    }
    return ret;
}

void serial_send_break(SerialState *s)
{
    s->rbr = 0;
    s->lsr |= UART_LSR_BI | UART_LSR_DR;
    serial_update_irq(s);
}

#if 0
static void serial_send_char(SerialState *s, int ch)
{
    s->rbr = ch;
    s->lsr |= UART_LSR_DR;
    serial_update_irq(s);
}

Serial.prototype.send_char_from_fifo = function()
{
    var fifo;

    fifo = s->rx_fifo;
    if (fifo != "" && !(s->lsr & UART_LSR_DR)) {
        s->send_char(fifo.charCodeAt(0));
        s->rx_fifo = fifo.substr(1, fifo.length - 1);
    }
}

/* queue the string in the UART receive fifo and send it ASAP */
Serial.prototype.send_chars = function(str)
{
    s->rx_fifo += str;
    s->send_char_from_fifo();
}
#endif
