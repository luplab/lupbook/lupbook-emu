/*
 * Interface for decompressing files
 *
 * Copyright (c) 2021 Garrett Hagopian
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef DECOMPRESS_H
#define DECOMPRESS_H

#include <stddef.h>

#include "cutils.h"

#ifdef CONFIG_DECOMP_LZ4
int unlz4(unsigned char *in, size_t in_len,
          unsigned char **out, size_t *out_len);
#else
static inline int unlz4(unsigned char *in, size_t in_len,
                        unsigned char **out, size_t *out_len)
{
    return 0;
}
#endif

#ifdef CONFIG_DECOMP_ZSTD
unsigned long long ZSTD_getFrameContentSize(const void *src, size_t srcSize);
size_t ZSTD_decompress(void *dst, size_t dstCapacity,
                       const void *src, size_t compressedSize);
unsigned ZSTD_isError(size_t code);

static inline int unzstd(unsigned char *in, size_t in_len,
                         unsigned char **out, size_t *out_len)
{
    void *_out;
    size_t d_size;
    ssize_t const _out_len = ZSTD_getFrameContentSize(in, in_len);

    if (_out_len < 0)
        return 0;

    _out = xmalloc(_out_len);
    d_size = ZSTD_decompress(_out, _out_len, in, in_len);
    if (ZSTD_isError(d_size)) {
        free(_out);
        return 0;
    }

    *out = _out;
    *out_len = _out_len;
    return 1;
}
#else
static inline int unzstd(unsigned char *in, size_t in_len,
                         unsigned char **out, size_t *out_len)
{
    return 0;
}
#endif

static inline int decompress(unsigned char *in, size_t in_len,
                             unsigned char **out, size_t *out_len)
{
    if (unlz4(in, in_len, out, out_len) || unzstd(in, in_len, out, out_len))
        return 1;
    return 0;
}
#endif /* DECOMPRESS_H */
