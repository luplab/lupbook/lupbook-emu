/*
 * VM utilities
 *
 * Copyright (c) 2017 Fabrice Bellard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <getopt.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "cutils.h"
#include "host.h"
#include "iomem.h"
#include "machine.h"
#include "virtio.h"

/* currently only for "TZ" */
static char *cmdline_subst(const char *cmdline)
{
    DynBuf dbuf;
    const char *p;
    char var_name[32], *q, buf[32];

    dbuf_init(&dbuf);
    p = cmdline;
    while (*p != '\0') {
        if (p[0] == '$' && p[1] == '{') {
            p += 2;
            q = var_name;
            while (*p != '\0' && *p != '}') {
                if ((q - var_name) < sizeof(var_name) - 1)
                    *q++ = *p;
                p++;
            }
            *q = '\0';
            if (*p == '}')
                p++;
            if (!strcmp(var_name, "TZ")) {
                time_t ti;
                struct tm tm;
                int n, sg;
                /* get the offset to UTC */
                time(&ti);
                localtime_r(&ti, &tm);
                n = tm.tm_gmtoff / 60;
                sg = '-';
                if (n < 0) {
                    sg = '+';
                    n = -n;
                }
                snprintf(buf, sizeof(buf), "UTC%c%02d:%02d",
                         sg, n / 60, n % 60);
                dbuf_putstr(&dbuf, buf);
            }
        } else {
            dbuf_putc(&dbuf, *p++);
        }
    }
    dbuf_putc(&dbuf, 0);
    return (char *)dbuf.buf;
}

static bool find_name(const char *name, const char *name_list)
{
    size_t len;
    const char *p, *r;

    p = name_list;
    for(;;) {
        r = strchr(p, ',');
        if (!r) {
            if (!strcmp(name, p))
                return true;
            break;
        } else {
            len = r - p;
            if (len == strlen(name) && !memcmp(name, p, len))
                return true;
            p = r + 1;
        }
    }
    return false;
}

static const VirtMachineClass *virt_machine_list[] = {
#if defined(EMSCRIPTEN)
    /* only a single machine in the EMSCRIPTEN target */
#ifndef CONFIG_X86EMU
    &riscv_machine_class,
#endif
#else
    &riscv_machine_class,
#endif /* !EMSCRIPTEN */
#ifdef CONFIG_X86EMU
    &pc_machine_class,
#endif
    NULL,
};

static const VirtMachineClass *virt_machine_find_class(const char *machine_name)
{
    const VirtMachineClass *vmc, **pvmc;

    for(pvmc = virt_machine_list; *pvmc != NULL; pvmc++) {
        vmc = *pvmc;
        if (find_name(machine_name, vmc->machine_names))
            return vmc;
    }
    return NULL;
}

static void help(const char *prog_name)
{
    printf("Usage: %s [options]\n\n", prog_name);
    printf("Options are:\n\
    -h                   Display this help message\n\
\n\
    -accel               KVM acceleration (only temu/x86)\n\
    -append <cmdline>    Linux command line\n\
    -bios <path>         BIOS\n\
    -drive <path>        File system on block device\n\
    -initrd <path>       Init file system in RAM\n\
    -kernel <path>       Kernel\n\
    -m <size>            Size of RAM (in MiB)\n\
    -machine <type>      Type of machine (e.g., 'riscv32' or 'pc')\n\
\n\
Console keys:\n\
    Press C-a x to exit the emulator, C-a h to get some help\n"
    );
}

void virt_machine_parse_args(VirtMachineParams *p, int argc, char *argv[])
{
    int i;

    /* Parse command-line arguments */
    for (;;) {
        int c, this_opt_optind;
        static struct option long_opts[] = {
            /* Admin options */
            {"help",    no_argument,        0, 'h'},
            /* Emulator options */
            {"accel",   no_argument,        0, 'a'}, /* KVM acceleration */
            {"append",  required_argument,  0, 'c'}, /* Command line */
            {"bios",    required_argument,  0, 'b'}, /* BIOS binary */
            {"drive",   required_argument,  0, 'd'}, /* FS binary */
            {"initrd",  required_argument,  0, 'i'}, /* Initrd binary */
            {"kernel",  required_argument,  0, 'k'}, /* Kernel binary */
            {"m",       required_argument,  0, 'm'}, /* Memory size */
            {"machine", required_argument,  0, 't'}, /* Machine type */
        };


        this_opt_optind = optind;
        c = getopt_long_only(argc, argv, "", long_opts, NULL);
        if (c == -1)
            /* No more options */
            break;

        switch (c) {
            case 'h': /* Get help */
                help(argv[0]);
                exit(0);

            case 'a': /* KVM acceleration */
                p->accel_enable = true;
                break;

            case 'c': /* Command line */
                p->cmdline = cmdline_subst(optarg);
                break;

            case 'b': /* BIOS binary */
                p->files[VM_FILE_BIOS].filename = strdup(optarg);
                break;

            case 'd': /* FS binary */
                p->fs_filename = strdup(optarg);
                break;

            case 'i': /* Initrd binary */
                p->files[VM_FILE_INITRD].filename = strdup(optarg);
                break;

            case 'k': /* Kernel binary */
                p->files[VM_FILE_KERNEL].filename = strdup(optarg);
                break;

            case 'm': /* Memory size */
                p->ram_size = (uint64_t)atoi(optarg) << 20;
                break;

            case 't': /* Machine type */
                if (this_opt_optind != 1)
                    die("Option `-machine` must be first");
                p->machine_name = strdup(optarg);
                p->vmc = virt_machine_find_class(p->machine_name);
                if (!p->vmc)
                    die("Unknown machine name: %s", p->machine_name);
                p->vmc->virt_machine_set_defaults(p);
                break;

            case '?':
            default:
                exit(1);
        }
    }

    /* Check config */
    if (!p->machine_name)
        die("Machine type undefined");

    /* Load binaries */
    for(i = 0; i < VM_FILE_COUNT; i++) {
        if (p->files[i].filename)
            p->files[i].len = load_file(&p->files[i].buf, p->files[i].filename);
    }
}

void virt_machine_free_config(VirtMachineParams *p)
{
    int i;

    free(p->machine_name);
    free(p->cmdline);
    for(i = 0; i < VM_FILE_COUNT; i++) {
        free(p->files[i].filename);
        free(p->files[i].buf);
    }
    free(p->fs_filename);
    free(p->cfg_filename);
}

VirtMachine *virt_machine_init(const VirtMachineParams *p)
{
    const VirtMachineClass *vmc = p->vmc;
    return vmc->virt_machine_init(p);
}

void virt_machine_reset_config(VirtMachineParams *p)
{
    memset(p, 0, sizeof(*p));
}

void virt_machine_end(VirtMachine *s)
{
    s->vmc->virt_machine_end(s);
}
