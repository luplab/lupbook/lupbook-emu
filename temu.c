/*
 * TinyEMU
 *
 * Copyright (c) 2016-2018 Fabrice Bellard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifdef EMSCRIPTEN
#include <emscripten.h>
#endif
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cutils.h"
#include "fs.h"
#include "host.h"
#include "machine.h"

int main(int argc, char *argv[])
{
    VirtMachine *s;
    VirtMachineParams *p;

    p = xmallocz(sizeof(VirtMachineParams));

    virt_machine_reset_config(p);
    virt_machine_parse_args(p, argc, argv);

    if (p->fs_filename) {
        FSDevice *fs = NULL;

        if (
#ifndef EMSCRIPTEN
        !(fs = fs_disk_init(p->fs_filename)) &&
#endif
        !(fs = fs_cpio_init(p->fs_filename))
        )
            die("'%s' invalid: not a CPIO archive"
#ifndef EMSCRIPTEN
                " or a directory"
#endif
                , p->fs_filename);

        p->fs_dev = fs;
    }

    p->console = console_init();
    p->rtc_real_time = true;

    s = virt_machine_init(p);

    virt_machine_free_config(p);

    host_init();

#ifdef EMSCRIPTEN
    emscripten_async_call(virt_machine_run, s, 0);
#else
    for(;;) {
        virt_machine_run(s);
    }
    virt_machine_end(s);
#endif

    return 0;
}
